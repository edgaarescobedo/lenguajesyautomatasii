#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include <QFile>

int isMultiplier(const QString &val){

    if(val == "*" || val == "/"){
        return 60;
    }

    return -1;

}

int isSum(const QString &val){
    if(val == "+" || val == "-"){
        return 50;
    }

    return -1;
}

int isRelational(const QString &val){
    if(val == "==" || val == "!=" || val == "<" || val == ">" || val == "<=" || val == ">="){
        return 40;
    }

    return -1;
}

bool isNumber(const QString &val){
    return val.at(0).isNumber();
}

bool isIf(const QString &val){
    return val == "if";
}

bool isElse(const QString &val){
    return val == "else";
}

bool isEntonces(const QString &val){
    return val == "then";
}

bool isBegin(const QString &val){
    return val == "begin";
}

bool isEnd(const QString &val){
    return val == "end";
}

int isAnd(const QString &val){
    if(val == "and"){
        return 20;
    } else {
        return -1;
    }
}

int isOr(const QString &val){
    if(val == "or"){
        return 10;
    } else {
        return -1;
    }
}

int isNot(const QString &val){
    if(val == "not"){
        return 30;
    } else {
        return -1;
    }
}

bool isVariable(const QString &val){
    return val.at(0).isLetter();
}

bool isString(const QString &val){
    if(val.at(0) == "“"){
        return true;
    } else {
        return false;
    }

}


int main()
{
    //Estructura de inicio
    QList<QPair<QString, QString>> variablesConValores;
    QList<QString> variables;
    QList<QPair<QString, int>> stack;
    QList<QString> estados;
    QList<QString> variables;
    QList<int> direcciones;
    QList<QString> vector;
    QString variablePrincipal;
    QString valorPrincipal;
    bool error = false;
    int i = 0;
    bool error = false;
    QString variablePrincipal = "";


    //Se crea el lector por consola
    QTextStream input(stdin);


    //INICIO
    qDebug() << "Ingrese la dirección del archivo";
    QFile file(input.readLine());

    //Se intenta abrir el archivo, en caso de que no se pueda entonces se tiene que
    //terminar el programa
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "No se pudo leer el archivo, puede que esté dañado";
        return 0;
    }
    //Se lee todo lo que contiene el archivo
    QString textInFile = file.readAll();

    //Expresión regular para eliminar espacios en blanco en general
    QRegExp exp("[\\s\\n\\r\\t]+");

    //Se hace un split del texto que se encuentra en el archivo con la expresión
    //regular especificada
    QList<QString> split = textInFile.split(exp);

    foreach(auto &val, split){

        //Si es un espacio en blanco se ignora
        if(val == " " || val == ""){
            continue;
        }

        if(val == "end"){
            qDebug() << "Se ha encontrado un end";
        }

        //Se aumenta el contador para saber la posición dentro del split
        ++i;

        qDebug() << "Ahora en val: " << val;
        qDebug() << "Valores en el stack: " << stack;

        //Si es begin se ignora
        if(isBegin(val)){
            continue;
        }

        //Si es if entonces se mete a la pila de estados
        if(isIf(val)){
            error = true;
            break;
            //estados.push_front(val);
            //continue;
        }

        //Si es entonces se crea el token falso y se crea el token then
        //se guarda la dirección del token falso
        if(isEntonces(val)){
            error = true;
            break;
            //vector.push_back("token falso");
            //direcciones.push_front(vector.size() - 1);
            //vector.push_back("then");
            //continue;
        }

        //Si es fin
        if(isEnd(val)){
            error = true;
            break;
            //qDebug() << "Se entro a end ";
            //qDebug() << "El valor de i es: " << i;
//            qDebug() << "El valor de split size es: " << split.size();
            //Si es seguido de un else se ignora y se quita el estado del si
//            if(i < split.size() && isElse(split.at(i))){
//                estados.pop_front();
//                qDebug() << "End seguido de un else";
//                continue;
//            }

            //Si no se quita el estatuto que se encuentre
//            estados.pop_front();
            //Se guarda la dirección del operador en la dirección del token falso
//            int dir = direcciones.takeFirst();
//            vector.replace(dir, "*" + QString::number(vector.size()));
//            continue;
        }

        //Si es else
        if(isElse(val)){
            error = true;
            break;
            //Se hace un push del estatuto del else
//            estados.push_front("else");

            //pop dirección y almacenar valor de op + 2
//            int dir = direcciones.takeFirst();
//            vector.replace(dir, "*"+QString::number(vector.size() + 2));

            //Generar token falso y almacenar nueva dirección en dir
//            vector.push_back("token falso");
//            direcciones.push_front(vector.size() - 1);
//            vector.push_back("else");

//            continue;
        }

        if(isAnd(val) > 0){
            //qDebug() << "Valor es un multiplicador: " << val;
            //Se verifica que el de arriba no tenga prioridad menor

            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isAnd(val)){

                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }


            stack.push_front(QPair<QString,int>(val, isAnd(val)));


            //qDebug() << "Valores en el stack: " << stack;

            //Se termina la ejecución del multiplo
            continue;
        }

        if(isOr(val) > 0){
            //qDebug() << "Valor es un multiplicador: " << val;
            //Se verifica que el de arriba no tenga prioridad menor

            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isOr(val)){

                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }


            stack.push_front(QPair<QString,int>(val, isOr(val)));


            //qDebug() << "Valores en el stack: " << stack;

            //Se termina la ejecución del multiplo
            continue;
        }

        if(isNot(val) > 0){
            //qDebug() << "Valor es un multiplicador: " << val;
            //Se verifica que el de arriba no tenga prioridad menor

            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isNot(val)){

                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }


            stack.push_front(QPair<QString,int>(val, isNot(val)));


            //qDebug() << "Valores en el stack: " << stack;

            //Se termina la ejecución del multiplo
            continue;
        }

        //Si es la variable entra al vector igual si es un numero
        if(isVariable(val) || isNumber(val) || isString(val)){
            vector.push_back(val);
            //qDebug() << "El valor fue una variable o numero: " << val;
            continue;
        }

        //Se checa si es un igualador y se mete en el stack con valor a 0
        if(val == "="){
            stack.push_front(QPair<QString,int>(val, 0));
            //qDebug() << "El valor fue un =";
            //qDebug() << "Valores en el stack: " << stack;
            continue;
        }

        //Si no es variable ni numero entonces se checa que es
        if(isMultiplier(val) > 0){

            //qDebug() << "Valor es un multiplicador: " << val;
            //Se verifica que el de arriba no tenga prioridad menor

            qDebug() << "Entrando a is multiplier";
            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isMultiplier(val)){

                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }


            stack.push_front(QPair<QString,int>(val, isMultiplier(val)));


            //qDebug() << "Valores en el stack: " << stack;

            //Se termina la ejecución del multiplo
            continue;
        }

        //Si no entró al multiplier entonces se checa si es una suma
        if(isSum(val)  > 0){
            //qDebug() << "Valor es un sum: " << val;
            //Se verifica que el de arriba no tenga prioridad menor
            //QPair<QString, int> pair = stack.front();



            //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isSum(val)){

                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }

            stack.push_front(QPair<QString,int>(val, isSum(val)));

            //qDebug() << "Valores en el stack: " << stack;
            //Se termina la ejecución del multiplo
            continue;
        }

        if(isRelational(val) > 0){
            //qDebug() << "Valor es un sum: " << val;
            //Se verifica que el de arriba no tenga prioridad menor
            //QPair<QString, int> pair = stack.front();

            if(!stack.isEmpty()){
                QPair<QString, int> pair = stack.front();

                //Si tiene prioridad menor entonces se tiene que sacar el que está en el stack y meter este
                while(pair.second >= isRelational(val)){
                    vector.push_back(pair.first);                   //Ingresando el token en el vector
                    stack.pop_front();                              //Se quita el tope

                    //Se actualiza el valor del pair
                    //Se actualiza el valor del pair
                    if(stack.isEmpty()){
                        break;
                    } else {
                        pair = stack.front();
                    }
                }
            }

            stack.push_front(QPair<QString,int>(val, isRelational(val)));

            //qDebug() << "Valores en el stack: " << stack;
            //Se termina la ejecución del multiplo
            continue;
        }

        //Si no entró a la suma entonces se verifica si es un parentesis que abre
        if(val == "("){
            //qDebug() << "Valor es un (";
            //Si es así entonces se ingresa en la pila con un valor de 0
            stack.push_front(QPair<QString,int>(val, 0));

            //qDebug() << "Valores en el stack: " << stack;
            continue;
        }

        //Si es un parentesis que cierra entonces se tiene que sacar todo de la pila hasta el
        //primer parentesis que abre
        if(val == ")"){
            //qDebug() << "Valor es un )";
            //Mientras no se haya encontrado el valor 0 en el stack se va a tener que seguir sacando
            //de el
            QPair<QString,int> pair = stack.front();
            //Mientras sea diferente a 0 se va a sacar y se va a poner en el vector
            while(pair.second != 0){
                vector.push_back(pair.first);
                stack.pop_front();
                pair = stack.front();
            }

            //Si se ha encontrado el 0 entonces solamente se saca del stack
            if(pair.second == 0){
                stack.pop_front();
            }

            //qDebug() << "Valores en el stack: " << stack;
            continue;
        }

        //Si se encontró el ; entonces quiere decir que la expresión ha terminado
        //y se tiene que sacar todo del stack
        if(val == ";"){
            //qDebug() << "Valor es un ;";
            while(!stack.isEmpty()){
                vector.push_back(stack.front().first);
                stack.pop_front();
                //qDebug() << "Valores en el stack: " << stack;
            }

            //Se agrega el valor del ; en el vector

        }

    }


    //Se muestra el vector resultante
    qDebug() << "Vector de código intermedio: " << vector;

    //Se tiene que checar si hubi algun error para terminar el programa
    if(!error){
        //Se empiezan a hacer las operaciones correspondientes
        //Recorriendo el VCI
        for(int i = 0; i < vector.size(); ++i){
            auto &ref = vector.at(i);

            //CONDICIONES PARA SALTO EN VECTOR DE EJECUCIÓN
            if(ref.at(0) == '*' && ref.size() > 1){
                //Se extrae la posición del token falso
                int pos = ref.right(ref.size()-1).toInt();

                //Si ya no quedan más valores en variables entonces solo se salta a ese lugar
                if(variables.isEmpty()){
                    i = pos-1;
                    qDebug() << "Recorriendo a la posición indicada";
                    continue;
                }

                //Si el ultimo valor de las variables es verdadero entonces solo saltamos dos lugares hacia adelante
                if(variables.takeLast().toInt()){
                    //Si nos salimos del vector de codigo intermedio se termina el programa
                    if(i+2 >= vector.size()){
                        qDebug() << "Se ha terminado la ejecución";
                        break;
                    }
                    //Se hace el salto dos lugares, saltando el token falso y la expresión
                    qDebug() << "Valor fue verdadero, recorriendo dos lugares hacia adelante";
                    i++;
                    continue;
                } else {
                    //En caso de que sea falso nos vamos al lugar en donde indica el token falso
                    i = pos-1;
                    qDebug() << "Valor fue falso, recorriendo a la posición indicada";
                    continue;
                }
            }

            //Solo puede haber una variable
            if(ref == "true" || ref == "false"){
                variables.append(ref);
                continue;
            } else if(isVariable(ref) && !(isAnd(ref) > 0 || isOr(ref) > 0 || isNot(ref) > 0)){
                variablePrincipal = ref;
                variables.append(ref);
                continue;
            }
            //Si es numero o variable se ingresa en el vector correspondiente
            if(isNumber(ref)){
                variables.append(ref);
                continue;
            } else {

                //Si es un igual no se hace nada de lo siguiente
                if(ref == "="){
                    QString val2 = variables.takeLast();
                    QString val1 = variables.takeLast();

                    valorPrincipal = val2;
                    qDebug() << variablePrincipal << " = " << valorPrincipal;
                    continue;
                }

                //Se tienen que sacar los valores correspondientes
                QString val2 = variables.takeLast();
                QString val1 = variables.takeLast();

                qDebug() << "Valores obtenidos del stack:";
                qDebug() << "Val1: " << val1;
                qDebug() << "Val2: " << val2;
                qDebug() << "Operador: " << ref;

                //Si alguno de los dos es variable principal entonces se cambia por su valor
                if(val1 == variablePrincipal){
                    val1 = valorPrincipal;
                }

                if(val2 == variablePrincipal){
                    val2 = valorPrincipal;
                }

                QString res = "";

                double realval1, realval2;

                //Se procede a realizar la operación
                //Solo aceptan doubles
                if(isRelational(ref) > 0){
                    if(ref == "=="){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 == realval2);
                        variables.append(res);
                    }else if(ref == "!="){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 != realval2);
                        variables.append(res);
                    }else if(ref == "<"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 < realval2);
                        variables.append(res);
                    }else if(ref == ">"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 > realval2);
                        variables.append(res);
                    }else if(ref == "<="){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 <= realval2);
                        variables.append(res);
                    }else if(ref == ">="){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 >= realval2);
                        variables.append(res);
                    }
                //Solo aceptan doubles
                }else if(isMultiplier(ref) > 0){
                    if(ref == "*"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 * realval2);
                        variables.append(res);
                    } else if (ref == "/"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 / realval2);
                        variables.append(res);
                    }
                //Solo aceptan doubles
                }else if(isSum(ref) > 0){
                    if(ref == "+"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 + realval2);
                        variables.append(res);
                    }else if(ref == "-"){
                        realval1 = val1.toDouble(&error);

                        realval2 = val2.toDouble(&error);

                        res = QString::number(realval1 - realval2);
                        variables.append(res);
                    }
                //Solo aceptan relacionales
                }else if(isAnd(ref) > 0){
                    bool realval1 = val1.toInt(&error);

                    bool realval2 = val2.toInt(&error);

                    res = QString::number(realval1 && realval2);
                    variables.append(res);
                }else if(isOr(ref) > 0){
                    bool realval1 = val1.toInt(&error);

                    bool realval2 = val2.toInt(&error);

                    res = QString::number(realval1 || realval2);
                    variables.append(res);
                }else if(isNot(ref) > 0){
                    qDebug() << "isNot not implemented yet";
                }else if(ref == "="){
                    qDebug() << "Entrando a una asignación en donde no debería de entrar";
                }

                qDebug() << "Resultado de la operación = " << res;
            }
        }

        if(0){
            qDebug() << "Error realizando las operaciones en el VCI";
        }

    } else {
        //Se muestra el vector resultante
        qDebug() << "Vector de código intermedio: " << vector;
    }

    qDebug() << "Programa terminado satisfactoriamente";
}
